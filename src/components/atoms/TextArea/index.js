import React from 'react'
import './textArea.scss'
const TextArea = ({...rest}) => {
    return (
        <div  {...rest}>
            <textarea className="text-area"></textarea>
            
        </div>
    )
}

export default TextArea
