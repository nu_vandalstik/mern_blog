import React from 'react';
import './input.scss';

function Input({label, ...rest}) {
    return (
        <div className="input-wrapper">
            <p>{label}</p>
            <input className="input"  {...rest} ></input>
        </div>
    )
}



export default Input
