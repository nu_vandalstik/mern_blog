import RegisterBg from './image/anete.jpg';
import LoginBg from './image/lukas.jpg';
import Rjs from './image/Rjs.png'
//icon

import ICFacebook from './icon/Facebook.svg'
import ICTwitter from './icon/twitter.png'
import ICInstagram from './icon/instagram.png'
import ICTelegram from './icon/telegram.png'
import ICDiscord from './icon/discord.png'
import ICGithub from './icon/gitlab.png'


export {RegisterBg, LoginBg, Rjs, ICFacebook,ICTwitter,ICInstagram,ICTelegram,
ICDiscord, ICGithub};