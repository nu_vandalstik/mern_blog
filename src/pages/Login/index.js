import React from 'react';
import { useHistory } from "react-router-dom";
import {LoginBg} from '../../assets';
import { Button, Gap, Input, Link } from "../../components";


const Login = () => {
  const history = useHistory();
    return (
             
    <div className="main-page">
    <div className="left">
      <img src={LoginBg} className="bg-image" alt="imageBg"/>
    </div>
    <div className="right">
      <p className="title"> Login</p>
      <Input label="Email" placeholder="Email"/>
      <Gap height ={18}></Gap>
      <Input label="password" placeholder="Password"/>
      <Gap height ={50}></Gap>
      <Button title="Login" onClick={()=> history.push('/')}></Button>
      <Gap height ={100}></Gap>
      <Link title="Belum Punya Akun, Silakahkan Daftar" onClick={()=> history.push('/register')}></Link>
    </div>
  </div>
    )
}

export default Login
