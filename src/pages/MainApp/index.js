import React from "react";
import DetailBlog from "../DetailBlog";
import Home from "../Home";
import CreateBlog from "../CreateBlog";
import { Footer, Header } from "../../components";
import "./mainApp.scss";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const MainApp = () => {
  return (
    <div className="main-app-wrapper">
      <Header></Header>

      <div className="content-wrapper">
        <Router>
          <Switch>
            <Route path="/create-blog">
              <CreateBlog></CreateBlog>
            </Route>
            <Route path="/detail-blog">
              <DetailBlog></DetailBlog>
            </Route>
            <Route path="/">
              <Home></Home>
            </Route>
          </Switch>
        </Router>
      </div>

      <Footer></Footer>
    </div>
  );
};

export default MainApp;
