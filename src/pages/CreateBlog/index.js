import React, { useState } from 'react'
import {  useHistory } from 'react-router-dom'
import { Button, Gap, Input, Link, TextArea, Upload } from '../../components'
import './createBlog.scss'

import axios from 'axios'

const CreateBlog = () => {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('')
    const [image, setImage] = useState('');
    const [imagePreview, setImagePreview] = useState(null)
    const history = useHistory();


    const onSubmit = () =>{
        console.log('title :', title);
        console.log('body : ', body);
        console.log('image : ', image);

        const data = new FormData();
        data.append('title', title);
        data.append('body', body);
        data.append('image', image)

        axios.post('http://localhost:4000/v1/blog/post', data,{
            headers: {
                'conten-type' : 'multipart/form-data'
            }
        }).then(res =>{
            console.log('post success', res);
        })
        .catch(err =>{
            console.log('err:',err);
        })
    }

    const onImageUpload = (e) =>{
        const file = e.target.files[0];
        setImage(file)
        setImagePreview(URL.createObjectURL(file))
    }
    return (
        <div className="blog-post">
            <Link title="Kembali" onClick={ ()=> history.push('/')}/>
            <p className="title">Create New Blog Post</p>
            <Input label="Post Title" value={title} 
            onChange= { (e) => setTitle(e.target.value)}></Input>
            <Upload onChange={(e)=> onImageUpload(e)}></Upload>
            <TextArea value={body} 
            onChange={(e)=> setBody(e.target.value)} img={imagePreview}></TextArea>
            <Gap height={20}></Gap>
            <div className="button-action">
            <Button title="save" onClick={onSubmit}></Button>
            </div>
            <Gap height={20}></Gap>
        </div>
    )
}

export default CreateBlog
