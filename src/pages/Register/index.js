import React from "react";
import './register.scss';
import {RegisterBg} from '../../assets';
import { Button, Gap, Input, Link } from "../../components";
import {useHistory} from 'react-router-dom'

const Register = () => {
  const history = useHistory();
  return (
    <div className="main-page">
      <div className="left">
        <img src={RegisterBg} className="bg-image" alt="imageBg"/>
      </div>
      <div className="right">
        <p className="title"> Register</p>
        <Input label="FullName" placeholder="Full name"/>
        <Gap height ={18}></Gap>
        <Input label="Email" placeholder="Email"/>
        <Gap height ={18}></Gap>
        <Input label="password" placeholder="Password"/>
        <Gap height ={50}></Gap>
        <Button title="Register" onClick={() => history.push('/register')}></Button>
        <Gap height ={100}></Gap>
        <Link title="Kembali ke Login" onClick={()=> history.push ('/login')}></Link>
      </div>
    </div>
  );
};

export default Register;
