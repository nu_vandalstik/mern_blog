import React from 'react'
import { RegisterBg } from '../../assets'
import { Gap, Link } from '../../components'
import './detailBlog.scss'
import {useHistory} from 'react-router-dom'

const DetailBlog = () => {
const history = useHistory()
    return (
        <div className="detail-blog-wrapper">
            <img className="img-cover" src={RegisterBg} alt="thumb"/>
            <p className="blog-title">Title</p>
            <p className="blog-author">author</p>
            <p className="blog-body"> content</p>
            <Gap height={20}></Gap>
            <Link title="Kembali Ke Home" onClick={()=> history.push('/')}></Link>
        </div>
    )
}

export default DetailBlog
