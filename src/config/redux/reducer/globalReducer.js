
const initialState= {
    
    name: 'Wisnu'
}

const globalReducer = (state = initialState, action)=>{
    if(action.type=== 'UPDATE_NAME'){
        return {
            ...state,
            name: 'Kristanto'
        }
    }
    return state;
}

export default globalReducer;